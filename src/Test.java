package src;

public class Test {

    public static void main(String[] args) {

        // Step 7 Codes
        
            // Create 3 car instances

        Car car1 = new Car("Mustang GT","Black",4);
        Car car2 = new Car("Ferrari","Red",5);
        Car car3 = new Car("Lamborgini","Yellow",3);

            // Display them
        car1.display();
        car2.display();
        car3.display();

        // Step 8 codes
            // I chose car 1 to drive and change some properties

            // Set some properties
        car1.setFuelCapacity(220);
        car1.setMaxSpeed(290);
        car1.setProductionYear(1999);
        
        car1.incrementGear();

        car1.drive(50,110);

        car1.incrementGear();
        car1.decrementGear();
        car1.decrementGear();
        car1.decrementGear();
        car1.decrementGear();
        car1.decrementGear();
        car1.decrementGear();
        car1.decrementGear();

        car1.drive(20,180);

        car1.display();


    }
    
}
