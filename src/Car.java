package src;

public class Car {

    // Constant Values

    private final int MIN_GEAR = 0;
    private final int MAX_GEAR = 5;

    // Attributes 

    private float odometer,fuelCapacity,maxSpeed,odotimer,averageSpeed;
    private String brand,color;
    private int gear,productionYear;

    // Constructors

    public Car(String brand){
        this.brand      = brand;
        this.odometer   = 0;
        this.odotimer   = 0;
    }

    public Car(String brand,String color){
        this.brand      = brand;
        this.color      = color;
        this.odometer   = 0;
        this.odotimer   = 0;
    }

    public Car(String brand,String color,int gear){
        this.brand      = brand;
        this.color      = color;
        this.gear       = gear;
        this.odometer   = 0;
        this.odotimer   = 0;
    }

    // Bahviours

    public void incrementGear(){
        if (this.gear < MAX_GEAR){
            this.gear ++;
            System.out.println("\nThe gear is increased!\nNew Gear: "+this.gear);
        }else{
            System.out.println("\nThe gear cannot be increased. The max gear is "+ this.MAX_GEAR + "\nCurrent Gear: " + this.gear);
        }   
    }

    public void decrementGear(){
        if (this.gear > MIN_GEAR){
            this.gear --;
            System.out.println("\nThe gear is decreased!\nNew Gear: "+this.gear);
        }else{
            System.out.println("\nThe gear cannot be decreased. The max gear is "+ this.MAX_GEAR + "\nCurrent Gear: " + this.gear);
        }  
    }

    public void drive(float travelledHours,float kmPerHour){
        float travelledWay = travelledHours * kmPerHour;
        this.odometer += travelledWay;
        this.odotimer += travelledHours;

        this.averageSpeed = this.odometer/ this.odotimer; 

    }

    public void display(){
        String carContent = "\n\n-------------> CAR <-------------"
                          + "\nBrand              : "+ this.brand
                          + "\nProduction Year    : "+ this.productionYear
                          + "\nColor              : "+ this.color
                          + "\nOdometer           : "+ this.odometer
                          + "\nOdotimer           : "+ this.odotimer
                          + "\nGear               : "+ this.gear
                          + "\nFuel Capacity      : "+ this.fuelCapacity
                          + "\nMax Speed          : "+ this.maxSpeed
                          + "\nAvg Speed          : "+ this.averageSpeed
                          + "\nGear               : "+ this.gear
                          + "\n--------------------------------------";

        System.out.println(carContent);
    }

    // Getter - Setters

	public float getOdometer() {
		return odometer;
	}

	public void setOdometer(float odometer) {
		this.odometer = odometer;
	}

	public float getFuelCapacity() {
		return fuelCapacity;
	}

	public void setFuelCapacity(float fuelCapacity) {
		this.fuelCapacity = fuelCapacity;
	}

	public float getMaxSpeed() {
		return maxSpeed;
	}

	public void setMaxSpeed(float maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getGear() {
		return gear;
	}

	public void setGear(int gear) {
		this.gear = gear;
	}

	public int getProductionYear() {
		return productionYear;
	}

	public void setProductionYear(int productionYear) {
		this.productionYear = productionYear;
	}
}