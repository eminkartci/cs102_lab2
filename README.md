# CS102 Lab2


The explanation of the lab is attached as "Lab02.pdf"

## Car Object

#### Properties
```Java
    private float   odometer,fuelCapacity,maxSpeed,odotimer,averageSpeed;
    private String  brand,color;
    private int     gear,productionYear;
```
#### Sample Case
```Java
public class Test {

    public static void main(String[] args) {


        Car car1 = new Car("Mustang GT","Black",4);

        car1.display();

        car1.setFuelCapacity(220);
        car1.setMaxSpeed(290);
        car1.setProductionYear(1999);
        
        // Increment - Decrement gear
        car1.incrementGear();

        // Drive 50 mins with 110 km/h
        car1.drive(50,110);

        // Increment - Decrement gear
        car1.incrementGear();
        car1.decrementGear();
        car1.decrementGear();
        car1.decrementGear();
        car1.decrementGear();
        car1.decrementGear();
        car1.decrementGear();
        car1.decrementGear();

        // Drive 20 mins with 180 km/h
        car1.drive(20,180);

        // Avg Speed = ((50 * 110) + (20*120)) / (20+50)
        car1.display();


    }
}
```

```
Author  : Emin Kartci
Date    : 5 March 2021
GitHub  : eminkartci
```